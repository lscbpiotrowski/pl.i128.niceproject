package pl.i128.niceproject;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import pl.i128.niceproject.entity.PathFile;
import pl.i128.niceproject.service.FileService;

/**
 * This UI is the application entry point. A UI may either represent a browser
 * window (or tab) or some part of an HTML page where a Vaadin application is
 * embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is
 * intended to be overridden to add component to the user interface and
 * initialize non-component functionality.
 */
@Theme("mytheme")
public class MyUI extends UI {

	@Override
	protected void init(VaadinRequest vaadinRequest) {

		Set<PathFile> pathFileList = new HashSet<>();
		Grid<PathFile> pathFileGrid = new Grid<>();
		FileService fileService = new FileService();

		final VerticalLayout layout = new VerticalLayout();
		layout.addComponent(new Label("Upload File by Lukasz Piotrowski lscbp"));

		Label totalValueLabel = new Label("0");
		totalValueLabel.setCaption("Total Size:");
		layout.addComponent(totalValueLabel);

		TextField inputField = new TextField();
		inputField.setCaption("Type you file destinity");

		Button addToPathButton = new Button("Add Path to List");

		addToPathButton.addClickListener(e -> {
			if (!inputField.getValue().isEmpty()) {
				pathFileList.add(new PathFile(inputField.getValue(), 0));
				pathFileGrid.getDataProvider().refreshAll();
			}
		});

		layout.addComponent(inputField);
		layout.addComponent(addToPathButton);

		Button countButton = new Button("Count");

		countButton.addClickListener(e -> {
			fileService.checkExistFilesAndGenerateSize(pathFileList);
			pathFileGrid.getDataProvider().refreshAll();
			totalValueLabel.setValue(String.valueOf(fileService.getFullSize()));
		});

		layout.addComponent(countButton);

		pathFileGrid.setItems(pathFileList);

		pathFileGrid.addColumn(PathFile::getPath).setCaption("Path");
		pathFileGrid.addColumn(PathFile::getSize).setCaption("Size");

		layout.addComponent(pathFileGrid);

		setContent(layout);

	}

	@WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
	@VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
	public static class MyUIServlet extends VaadinServlet {
	}
}
