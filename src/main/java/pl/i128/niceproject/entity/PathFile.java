package pl.i128.niceproject.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
@EqualsAndHashCode
public class PathFile {

	private static final long serialVersionUID = 2501337300925909549L;

	@NonNull
	private String path;

	@NonNull
	private long size;

}
